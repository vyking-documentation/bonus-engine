# Bonus Engine Integration Documentation
![Version](https://img.shields.io/badge/Version-2.7.5-informational) ![Status](https://img.shields.io/badge/Status-published-green)

![Vyking Logo](https://www.vyking.com/assets/images/vyking_rgb_logo-farbe-154x50.png)

[TOC]

# General
High-level information on the bonus engine can be found on the [Vyking Website](https://www.vyking.com/products/bonus-engine). 
This product ships as part of the vyking platform, yet it can be integrated as stand-alone blackbox-like piece of 
software that sits on top of 3rd party PAMs (»_Player Account Management Platforms_«) or RGSs (»_Remote Gaming 
Systems_«). 

This documentation focuses primarily on instances where the bonus engine is being used by 3rd party PAMs and RGSs. 
To explore its features and functionality as part of the vyking platform, the following access credentials can be used. 

Please note that the workflows being displayed here are a simplification for visualization purposes. The respective 
endpoints to the various APIs quote here will provide several more methods for the purpose of handling edge cases 
such as system failure or unavailability of any counterparty in the workflow, or to obtain information needed for 
transaction related systems' communication. 

## Access Credentials
Please note that this is a staging environment where not everything always works a 100% and new features are being 
perpetually released for testing. 

Admin Panel - Stage:
- URL: https://admin-panel-demo.vyking.com/
- Username: partner.demo
- Password: test12345

Website - Stage:
- URL: https://demo.vyking.com/
- Username: capdemovky
- Pass: test12345

## Frontend API documentation
[Here](https://api.vyking.com/index.php?r=api%2Fclient) is the link to the frontend API documentation. Logging in with 
(partner.demo / test12345) will grant access to the »_admin-info_« endpoint which one could build an entire admin 
panel around. Vykings reference implementation (»URL above: https://admin-panel-demo.vyking.com/«) is using that. 

The »_client-info_« websocket based interface is a single endpoint to implement the bonus functionality into any 
website. 
![WebSocket APIs](/src/images/websocket-apis.png)

# Worflows
## General
There are 3 types of workflows subject to this document:
- User registration and login, token delivery
- Transacting funds, tying deposits to bonuses, gameplay and bonus funds clearing 
- Casino content sharing

## User registration
[User registration](/src/APIs/player_registration_openapi.yaml)

```mermaid
sequenceDiagram
    actor player
    box partner
        participant frontend
        participant backend
    end
    box vyking
        participant restAPI

        participant websocketAPI
    end

    autonumber
    alt new player is registering
        player ->> frontend: registration request
        frontend -->> backend: register player
        backend ->> restAPI: register player on vyking
        restAPI ->> backend: return playerID and token
        backend -->> frontend: logging in player
    else registered player is logging in
        player ->> frontend: login request
        frontend -->> backend: login request
        backend ->> restAPI: get vyking token
        restAPI ->> backend: return token
        backend -->> frontend: logging in player
    end
    frontend ->> websocketAPI: authenticate socket connection with vyking token
    websocketAPI ->> frontend: auth OK
    loop real time
        frontend -> websocketAPI: subscribe to real-time events, get content
    end
     
```

## Transacting funds
[Events API](/src/APIs/events_openapi.yaml)

```mermaid
sequenceDiagram
    actor player
    box partner
        participant frontend
        participant backend
    end
    box vyking
        participant restAPI
        participant websocketAPI
        participant bonusEngine
    end
    participant casinoStudio
    autonumber
    player ->> frontend: opens deposit page, chooses amount and method
    frontend ->> websocketAPI: get list of bonuses
    websocketAPI -->> bonusEngine: query templates
    bonusEngine -->> websocketAPI: return templates
    websocketAPI ->> frontend: return eligible bonuses for payment method and amount
    frontend ->> player: offer selection of bonuses to be used
    player ->> frontend: confirm bonus selection and execute deposit
    frontend -->> backend: process payment
    backend -->> frontend: return success
    frontend ->> websocketAPI: activate selected bonus template
    websocketAPI -->> bonusEngine: attempting template activation
    bonusEngine -->> restAPI: retrieve player funds
    restAPI ->> backend: start transaction
    backend ->> restAPI: returning funds
    restAPI -->> bonusEngine: activate template
    bonusEngine -->> websocketAPI: activation success
    websocketAPI ->> frontend: bonus activated
    Note over websocketAPI, frontend: At this point, balance is captured inside the bonus
    loop game play
        player ->> casinoStudio: place bet
        casinoStudio ->> backend: execute bet
        opt if bonus active
            backend ->> restAPI: execute bet
            restAPI -->> bonusEngine: confirming bet placement
            bonusEngine -->> bonusEngine: internal computations
            bonusEngine -->> restAPI: bet OK
            restAPI ->> backend: bet confirmed
        end
        backend ->> casinoStudio: bet confirmed
        casinoStudio ->> player: game result
    end
    alt bonus expired / wagering not met / funds lost
        bonusEngine ->> websocketAPI: bonus status
        bonusEngine ->> bonusEngine: close bonus account
    else wagering met / excess funds need to be returned
        bonusEngine -->> websocketAPI: bonus status
        bonusEngine -->> restAPI: return funds
        restAPI ->> backend: returning left over funds
        backend ->> restAPI: confirming transaction OK
        restAPI --> bonusEngine: transaction result OK
        bonusEngine -->> bonusEngine: close bonus account
    end

```

### Assign Bonus
```mermaid
sequenceDiagram
    actor player
    box partner
        participant frontend
        participant backend
    end
    box vyking
        participant restAPI
        participant websocketAPI
        participant bonusEngine
    end
    autonumber
    player ->> frontend: opens deposit page, chooses amount and method
    frontend ->> websocketAPI: get list of bonuses
    websocketAPI -->> bonusEngine: query eligibel bonuses
    bonusEngine -->> websocketAPI: return bonuses
    websocketAPI ->> frontend: return eligible bonuses for payment method and amount
    frontend ->> player: offer selection of bonuses to be used
    player ->> frontend: confirm bonus selection and execute deposit
    frontend -->> backend: process payment
    backend -->> frontend: return success
    backend ->> restAPI: activate selected bonus 
    restAPI -->> bonusEngine: attempting activation
    bonusEngine -->> restAPI: retrieve player funds
    restAPI ->> backend: start transaction
    backend ->> restAPI: returning funds
    restAPI -->> bonusEngine: activate bonus for player
    bonusEngine -->> websocketAPI: activation success
    websocketAPI ->> frontend: bonus activated notification
```

### Bonus Status | Cancel active Bonus
```mermaid
sequenceDiagram
    actor player
    box partner
        participant frontend
    end
    box vyking
        participant websocketAPI
        participant bonusEngine
    end
    autonumber
    player ->> frontend: opens bonus page
    frontend ->> websocketAPI: get list of active bonuses
    websocketAPI ->> bonusEngine: query active bonuses
    bonusEngine ->> websocketAPI: return bonuses details
    websocketAPI ->> frontend: return bonus details
    frontend ->> player: show bonus details
    alt cancel active bonus
        player ->> frontend: confirm bonus cancel
        frontend ->> websocketAPI: bonus cancel request
        websocketAPI ->> bonusEngine: remove bonus balance and close bonus
        bonusEngine ->> websocketAPI: confirm cancel and closed bonus
        websocketAPI ->> frontend: return success
        frontend ->> player: show success
        websocketAPI ->> frontend: bonus changed notification
    end
```

### Game Play
```mermaid
sequenceDiagram
    actor player
    
    box partner
        participant frontend
        participant backend
    end
    
    box 3party Studio
        participant gameEngine
    end
    
    box vyking
        participant restAPI
        participant bonusEngine
    end

    autonumber
    loop game play
        player ->> gameEngine: place bet
        gameEngine ->> backend: execute bet
        opt if bonus active
            backend ->> restAPI: execute bet
            restAPI -->> bonusEngine: confirming bet placement
            bonusEngine -->> bonusEngine: internal computations
            bonusEngine -->> restAPI: bet OK
            restAPI ->> backend: bet confirmed
        end
        backend ->> gameEngine: bet confirmed
        gameEngine ->> player: game result
    end
```

### Bonus finished - condition met or expired
```mermaid
sequenceDiagram
    actor player
    
    box partner
        participant frontend
        participant backend
    end
        
    box vyking
        participant restAPI
        participant bonusEngine
        participant websocketAPI
    end

    player ->> frontend: login
    frontend ->> websocketAPI: login
    autonumber
    loop
        bonusEngine ->> bonusEngine: check bonus conditions
        alt bonus expired / wagering not met / funds lost
            bonusEngine ->> bonusEngine: close bonus account
            bonusEngine -->> websocketAPI: notify bonus changed
        else wagering met / excess funds need to be returned
            bonusEngine ->> restAPI: return funds
            restAPI ->> backend: returning left over funds
            backend ->> restAPI: confirming transaction OK
            restAPI -> bonusEngine: transaction result OK
            bonusEngine ->> bonusEngine: close bonus account
            bonusEngine -->> websocketAPI: notify bonus changed
        end
    end
    websocketAPI -->> frontend: notification
    frontend -->> player: notifcation

```


## Casino content and reverse integration
Considering that the partner is host to the Casino content integrations, a [reverse integration](https://developers.vyking.com/docs/reverse-casino-integration/peop4f7v1pfeq-overview) 
is required. The partner will act towards vyking not only as customer to use the bonus engine as defined in 
previous workflows, but moreover also as supplier to the casino content it has integrated. The bonus engine will 
utilize that information to compute the bonus workings and configure templates accordingly. 

The main reasons for the need of the reverse integration is the ability for vyking to fetch a list of games 
available and add pertinent configuration to the games such as limiting their bonus contribution or restricting 
their usage altogether in the context of the bonus.  

```mermaid
sequenceDiagram
    participant casinoStudio
    box partner
        participant backend
        participant frontend
    end
    box vyking
        participant restAPI
        participant websocketAPI
    end
    autonumber
    casinoStudio -> backend: integration
    frontend -->> backend: list games
    backend -->> frontend: render games list
    backend ->> restAPI: share game list
    opt if bonus active
        frontend -->> frontend: remove games
        frontend ->> websocketAPI: request games list
        websocketAPI ->> frontend: return eligible games
        frontend -->> frontend: render eligible games
    end
```

# Limitations
If the bonus engine is a used as a blackbox within a 3rd party PAM / RGS, there are few limitations in functionality 
to simplify the integration process for vyking and its partners. The features unavailable are: 
- Non-Stick Bonuses are not available. 
- No split bets between real-money and bonus-money if partner maintains a real money balance with the player. 

